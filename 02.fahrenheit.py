"""
function(C):
    F = C * 1.8 + 32
    return F

Features - Inputs to our model
Labels - The output our model predicts
Example - A pair of inputs/outputs
"""

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

# Create the features (inputs)
celsius_q = np.array([-40, -10, 0, 8, 15, 22, 38], dtype=float)
fahrenheit_a = np.array([-40, 14, 32, 46, 59, 72, 100], dtype=float)

# Print the value pairs
for i, c in enumerate(celsius_q):
    print("{} degrees Celsius = {} degrees Fahrenheit".format(c, fahrenheit_a[i]))

# Create a new single dense layer
l0 = tf.keras.layers.Dense(units=1, input_shape=[1])

# Assemble layers into the model
model = tf.keras.Sequential([l0])

# Compile the model
model.compile(loss="mean_squared_error", optimizer=tf.keras.optimizers.Adam(0.1))

# Train the model with one dense layer
history = model.fit(celsius_q, fahrenheit_a, epochs=500, verbose=False)
print("Finished training the model")

# Plot the graph with loss data
plt.xlabel("Epoch Number")
plt.ylabel("Logg Magnitude")
plt.plot(history.history["loss"])
# plt.show()

# Make a prediction
print(model.predict([100.0]))

# Print the layer variables
print("These are the layer variables: {}:".format(l0.get_weights()))

# Training the model with three dense layers
l0 = tf.keras.layers.Dense(units=4, input_shape=[1])
l1 = tf.keras.layers.Dense(units=4)
l2 = tf.keras.layers.Dense(units=1)
model = tf.keras.Sequential([l0, l1, l2])
model.compile(loss="mean_squared_error", optimizer=tf.keras.optimizers.Adam(0.1))
model.fit(celsius_q, fahrenheit_a, epochs=500, verbose=False)
print("")
print("Finished training the model with one layer")
print("")

# Make a prediction
print(model.predict([100.0]))
print("")

# Add more layers
print("Model predicts that 100 degrees Celsius is: {} degrees Fahrenheit".format(model.predict([100.0])))
print("")
print("These are the l0 variables: {}".format(l0.get_weights()))
print("")
print("These are the l1 variables: {}".format(l1.get_weights()))
print("")
print("These are the l2 variables: {}".format(l2.get_weights()))

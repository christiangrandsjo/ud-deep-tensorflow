# Import libraries (plt, np, tf)


# Create features (celsius_q, fahrenheit_a)
# [-40, -10, 0, 8, 15, 22, 38]
# [-40, 14, 32, 46, 59, 72, 100]


# Print the value pairs (enumerate)


# Create a dense layer (units, input_shape)


# Assemble layers into the model (Sequential)


# Compile the model (loss, optimizer)


# Train the model with one dense layer (c, f, ep, verb)


# Plot the graph with loss data (epoch number, logg magnitude, loss)


# Make a prediction for 100 C


# Print the layer variables (get_weights())

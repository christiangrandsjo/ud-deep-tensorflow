import time

import matplotlib.pylab as plt
import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds
import tensorflow_hub as hub
from keras import layers

tfds.disable_progress_bar()
splits = tfds.Split.ALL.subsplit(weighted=(80, 20))
splits, info = tfds.load("cats_vs_dogs", with_info=True, as_supervised=True, split=splits)
(train_examples, validation_examples) = splits


def format_image(image, label):
    # `hub` image modules exepct their data normalized to the [0,1] range.
    image = tf.image.resize(image, (IMAGE_RES, IMAGE_RES)) / 255.0
    return image, label


num_examples = info.splits["train"].num_examples
BATCH_SIZE = 32
IMAGE_RES = 224
train_batches = train_examples.cache().shuffle(num_examples // 4).map(format_image).batch(BATCH_SIZE).prefetch(1)
validation_batches = validation_examples.cache().map(format_image).batch(BATCH_SIZE).prefetch(1)

URL = "https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4"
feature_extractor = hub.KerasLayer(URL, input_shape=(IMAGE_RES, IMAGE_RES, 3))
feature_extractor.trainable = False

dense_layer = tf.keras.layers.Dense(2, activation=tf.nn.softmax)
model = tf.keras.Sequential([feature_extractor, dense_layer])
print(model.summary())
model.compile(optimizer="adam", loss=tf.losses.SparseCategoricalCrossentropy(), metrics=["accuracy"])

# EPOCHS = 3
EPOCHS = 2
history = model.fit(train_batches, epochs=EPOCHS, validation_data=validation_batches)


class_names = np.array(info.features["label"].names)
print(class_names)

image_batch, label_batch = next(iter(train_batches.take(1)))
image_batch = image_batch.numpy()
label_batch = label_batch.numpy()

predicted_batch = model.predict(image_batch)
predicted_batch = tf.squeeze(predicted_batch).numpy()
predicted_ids = np.argmax(predicted_batch, axis=-1)
predicted_class_names = class_names[predicted_ids]

print(predicted_class_names)
print("Labels: ", label_batch)
print("Predicted labels: ", predicted_ids)

plt.figure(figsize=(10, 9))
for n in range(30):
    plt.subplot(6, 5, n + 1)
    plt.imshow(image_batch[n])
    color = "blue" if predicted_ids[n] == label_batch[n] else "red"
    plt.title(predicted_class_names[n].title(), color=color)
    plt.axis("off")
_ = plt.suptitle("Model predictions (blue: correct, red: incorrect)")
plt.show()


t = time.time()
export_path_keras = "./{}.h5".format(int(t))
print(export_path_keras)
model.save(export_path_keras)

reloaded = tf.keras.models.load_model(
    export_path_keras,
    # `custom_objects` tells keras how to load a `hub.KerasLayer`
    custom_objects={"KerasLayer": hub.KerasLayer},
)

print(reloaded.summary())


result_batch = model.predict(image_batch)
reloaded_result_batch = reloaded.predict(image_batch)

print((abs(result_batch - reloaded_result_batch)).max())

# EPOCHS = 3
EPOCHS = 1
history = reloaded.fit(train_batches, epochs=EPOCHS, validation_data=validation_batches)

t = time.time()

export_path_sm = "./{}".format(int(t))
print(export_path_sm)
tf.saved_model.save(model, export_path_sm)
reloaded_sm = tf.saved_model.load(export_path_sm)
reload_sm_result_batch = reloaded_sm(image_batch, training=False).numpy()
print((abs(result_batch - reload_sm_result_batch)).max())

model = tf.keras.models.clone_model(model)
t = time.time()
export_path_sm = "./{}".format(int(t))
print(export_path_sm)
tf.saved_model.save(model, export_path_sm)

reload_sm_keras = tf.keras.models.load_model(export_path_sm, custom_objects={"KerasLayer": hub.KerasLayer})
print(reload_sm_keras.summary())

result_batch = model.predict(image_batch)
reload_sm_keras_result_batch = reload_sm_keras.predict(image_batch)

print((abs(result_batch - reload_sm_keras_result_batch)).max())
